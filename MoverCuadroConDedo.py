from handDetector import HandDetector
import cv2
import math
import numpy as np
from ctypes import cast, POINTER
ban = False
_x1 = 50
_y1 = 50
_x2=100
_y2=100

handDetector = HandDetector(min_detection_confidence=0.7)
webcamFeed = cv2.VideoCapture(2)

while True:
    status, image = webcamFeed.read()
    handLandmarks = handDetector.findHandLandMarks(image=image, draw=False)
    cv2.rectangle(image, (_x1,_y1), (_x2,_y2), (0,0,255), 3)
    if(len(handLandmarks) != 0):
        x1, y1 = handLandmarks[4][1], handLandmarks[4][2]
        x2, y2 = handLandmarks[8][1], handLandmarks[8][2]
        length = math.hypot(x2-x1, y2-y1)
        print(length)

        #cv2.circle(image, (x1, y1), 15, (255, 0, 255), cv2.FILLED)
        #cv2.circle(image, (x2, y2), 12, (255, 0, 255), cv2.FILLED)
        #cv2.line(image, (x1, y1), (x2, y2), (255, 0, 255), 3)
        if x2>=_x1 and x2 <= (_x1+50) and y2>=_y1 and y2<=(_y1+100):
               ban = True
        if ban == True and length>50:
               _x1=x2
               _y1=y2
               _x2=x2+50
               _y2=y2+50        
               cv2.rectangle(image, (_x1,_y1), (_x2,_y2), (0,0,255), 3)
        else:
               ban = False

    cv2.imshow("Volume", image)
    cv2.waitKey(1)
